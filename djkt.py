import datetime
import calendar
from operator import index
import requests
from selectolax.parser import HTMLParser
import locale
import sys
import os

locale.setlocale(locale.LC_TIME, "cs_CZ.UTF-8")

year = int(sys.argv[1])
month = int(sys.argv[2])
days_in_month = calendar.monthrange(year, month)[1]


def get_url(venue_id):
    template = "https://www.djkt.eu/program?filter%5Bdate_from%5D=DATE_FROM&\
                filter%5Bdate_to%5D=DATE_TO&filter%5Bvenue_id%5D=VENUE_ID&\
                filter%5Bsoubor%5D=&filter%5Bperson_id%5D=&filter%5Bsearch%5D="
    date_from = datetime.date(year, month, 1).strftime("%d.+%m.+%Y")
    date_to = datetime.date(year, month, days_in_month).strftime("%d.+%m.+%Y")
    return (
        template.replace("VENUE_ID", str(venue_id))
        .replace("DATE_FROM", date_from)
        .replace("DATE_TO", date_to)
    )


programme = {
    "vd": requests.get(get_url(1)).content,
    "ns": requests.get(get_url(2)).content,
    "ms": requests.get(get_url(3)).content,
}

shows = {}
for scene in programme:
    tree = HTMLParser(programme[scene])
    rows = tree.css(
        ".row.ensemble-,\
        .row.ensemble-cinohra,\
        .row.ensemble-muzikal,\
        .row.ensemble-opera,\
        .row.ensemble-balet"
    )
    shows[scene] = []
    for row in rows:
        links = row.css(".col-event a")
        for link in links:
            if link.attributes["href"].startswith("/"):
                link.attrs["href"] = f"https://www.djkt.eu{link.attributes['href']}"
        show = {
            "title": row.css_first(".title h3").text().strip(),
            "html": row.css_first(".col-event").html,
            "class": row.attributes["class"],
            "date": datetime.datetime.strptime(
                row.css_first(".date").text().strip() + " 2022", "%d. %m. %Y"
            ).date(),
        }
        try:
            show["tickets"] = row.css_first(".col-buttons form").html
        except AttributeError:
            show["tickets"] = ""

        shows[scene].append(show)

html = open("head.html", "r").read()
index = html
html += """<header>
            <div class="empty"></div>
            <div class="vd">Velké divadlo</div>
            <div class="ns">Nová scéna</div>
            <div class="ms">Malá scéna</div>
          </header>"""

for day in [
    datetime.date(year, month, day_num) for day_num in range(1, days_in_month + 1)
]:
    shows_html = ""
    for scene in shows:
        day_scene_shows = [show for show in shows[scene] if show["date"] == day]
        day_scene_html = ""
        for day_scene_show in day_scene_shows:
            day_scene_html += f"""<div class='{day_scene_show['class']}'>
                                      {day_scene_show['html']}{day_scene_show['tickets']}
                                  </div>"""
        shows_html += f"""<div class='shows scene-{scene}'>
                              {day_scene_html}
                          </div>"""

    html += f"""<div class='day'>
                    <div class='date'>{day.strftime('%A<br/>%-d. %-m. %Y')}</div>
                    {shows_html}
                </div>"""

html += "</body></html>"
title = datetime.date(year, month, 1).strftime("DJKT · %Y-%m")
html = html.replace("<title>Program DJKT</title>", f"<title>{title}</title>")

filename = datetime.date(year, month, 1).strftime("%Y-%m.html")
outfile = open(f"html/{filename}", "w")
outfile.write(html)
outfile.close()

index += "<ul>"
for filename in os.listdir("html"):
    if filename.startswith("2"):
        index += f"<li><a href='{filename}'>{filename.replace('.html', '')}</a></li>"
index += "</ul></body></html>"

indexfile = open(f"html/index.html", "w")
indexfile.write(index)
indexfile.close()
