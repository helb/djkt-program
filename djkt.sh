#!/bin/bash

source .venv/bin/activate

python djkt.py $(date "+%Y %m")
python djkt.py $(date "+%Y %m" -d "+1 month")
python djkt.py $(date "+%Y %m" -d "+2 months")